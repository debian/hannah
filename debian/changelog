hannah (2.0.1+ds1-0.3) unstable; urgency=medium

  * Non-maintainer upload.
  * refresh upstream copyright year as seen inside the game
  * allow cross-building (Closes: #1063394)
  * clean more (Closes: #1044187)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 07 Feb 2024 20:48:43 +0100

hannah (2.0.1+ds1-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Install data in expected location.

 -- Alexandre Detiste <tchet@debian.org>  Sat, 16 Dec 2023 03:19:50 +0100

hannah (2.0.1+ds1-0.1) unstable; urgency=medium

  * Non-maintainer upload.

  * New upstream version 2.0.1
  * build with SDL2 (Closes: #1038454)
  * fix homepage URL (Closes: #787934, #799547)

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Add Vcs-* field
  * d/watch: Use https protocol

 -- Alexandre Detiste <tchet@debian.org>  Sat, 16 Dec 2023 01:30:41 +0100

hannah (1.0-3.3) unstable; urgency=medium

  * Non-maintainer upload.

  [ Helmut Grohne ]
  * Fix FTCBFS: Let dh_auto_build pass cross tools to make.
    (Closes: #901260)

 -- Vagrant Cascadian <vagrant@reproducible-builds.org>  Thu, 06 Oct 2022 17:14:03 -0700

hannah (1.0-3.2) unstable; urgency=medium

  * Non-maintainer upload.

  [ Reiner Herrmann ]
  * Sort source files so that objects are linked in deterministic order
    (Closes: #845782)

 -- Vagrant Cascadian <vagrant@reproducible-builds.org>  Thu, 06 Oct 2022 16:48:01 -0700

hannah (1.0-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 12:25:33 +0100

hannah (1.0-3) unstable; urgency=medium

  * Move from dpatch to quilt.
  * Migrate to modern debhelper packaging.
  * Update Standards-Version to 3.9.6
    - No changes needed.
  * Explicitly set debian/source/format to 3.0 (quilt).
  * Moved to new copyright format.
  * Updated to debhelper compat 9.

 -- Margarita Manterola <marga@debian.org>  Sun, 07 Jun 2015 19:03:18 +0200

hannah (1.0-2) unstable; urgency=low

  * Corrected some lintian warnings:
    + Fixed a typo in the package description.
    + Added a description to the fix_score_dir patch.
    + Removed deprecated "Encoding" field from hannah.desktop
  * Updated Standards-Version to 3.8.0
    + Added README.source pointing to dpatch's README.source.gz

 -- Margarita Manterola <marga@debian.org>  Wed, 27 Aug 2008 14:34:40 +0000

hannah (1.0-1) unstable; urgency=low

  * New upstream release.
    + Fixed licensing for font.ttf and subgamefont.ttf. Thanks to Stan and
      Ray for re-licensing their fonts under the OFL!!
      (Closes: #470079, #470082).
  * Moved "Homepage" to new control field, updated the URL.
  * Updated Standards-Version to 3.7.3.
    + Changed menu section from Games/Arcade to Games/Action

 -- Margarita Manterola <marga@debian.org>  Sun, 27 Apr 2008 03:46:37 +0000

hannah (0.2.d-1) unstable; urgency=low

  * New upstream release
  * Dropped the previous patches since upstream fixed the Makefile.
  * Added a patch to keep using /var/games/hannah/ as the SCOREDIR
  * Move data files to /usr/share/games/hannah instead of /usr/share/hannah

 -- Margarita Manterola <marga@debian.org>  Sun, 27 Apr 2008 03:45:55 +0000

hannah (0.2.a-2) unstable; urgency=low

  * Fixed rules and filenames so that menu and desktop files get installed.
    Thanks to Yann Dirson for reporting this. (Closes: #412288)

 -- Margarita Manterola <marga@debian.org>  Wed, 28 Feb 2007 12:59:46 -0300

hannah (0.2.a-1) unstable; urgency=low

  * Initial release (Closes: #405753)
  * Patched the sources so that they allow "EXTRA_CFLAGS".
  * Patched the sources so that they look for a DATADIR and SCOREDIR.
  * Wrote a manpage.
  * Added a .desktop file.

 -- Margarita Manterola <marga@debian.org>  Mon, 15 Jan 2007 10:55:01 -0300
